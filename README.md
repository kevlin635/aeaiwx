**AEAI WX微信扩展框架**
-------------
AEAI WX微信扩展框架是基于Java封装的微信公众号二次开发框架，基于该框架可以快速接入微信，实现自定义菜单创建、信息按规则自动回复、集成企业的线上系统（HR、CRM、微店、网站等）、同时可以整合集成互联网开放资源（如：百度地图、天气预报、热映电影等）。

AEAI WX的框架包括两个工程aeaiwx_core（java工程）、aeaiwx（java web工程），架构框图如下所示：

![enter image description here](http://www.agileai.com/HotServer/reponsitory/images/oschina/wx.jpg)

AEAI WX提供嵌入使用、独立使用两种模式。嵌入使用模式直接把aeaiwx相关jar包放置于目标JavaWeb应用，一般只有一个系统使用AEAI WX微信框架建议使用嵌入使用模式。独立使用则是把AEAI WX微信框架独立出来专门作为一个Application甚至是一个Server，AEAI WX采用接口调用、共享内存方式跟业务系统交互，实现跟业务系统松耦合，可扩展集成多个业务系统。

独立使用模式预置了一些AEAI WX扩展样例，一旦掌握这些样例，完全可以根据实际场景需要调用对应微信Open API实现相关集成功能。具体可以参见AEAI WX微信扩展框架技术手册。

下面是AEAI WX微信扩展框架效果展示：

![enter image description here](http://www.agileai.com/HotServer/reponsitory/images/oschina/wxex1.jpg)
![enter image description here](http://www.agileai.com/HotServer/reponsitory/images/oschina/wxex2.jpg)
![enter image description here](http://www.agileai.com/HotServer/reponsitory/images/oschina/wxex3.jpg)

 **AEAI WX微信扩展框架特性：**
-------------
 1.	实用性：满足微信公众号扩展开发常见需求；
 2.	易用性：配置简单，10分钟完成接入，看到效果；
 3.	扩展性：良好的扩展性，修改配置、重写父类实现扩展。

欢迎加入数通畅联产品QQ技术群299719834一起参与讨论，也可以关注“数通畅联”微信公众号观看产品效果。官方网站：http://www.agileai.com