package com.agileai.weixin.bizmoduler.system;

import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;

public class SecurityRoleTreeSelectImpl
        extends TreeSelectServiceImpl
        implements SecurityRoleTreeSelect {
    public SecurityRoleTreeSelectImpl() {
        super();
    }
}
